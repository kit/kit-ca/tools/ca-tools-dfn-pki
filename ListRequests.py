#!.env/bin/python3

# Import libs
from argparse import ArgumentParser
from datetime import datetime
from suds import WebFault
from sys import stderr
import DFNClient
import Toolbox


# Set some constants
description = 'list certificate requests'


# Accept int or a dash
def request(input):
    return Toolbox.is_int_or_dash(input)


# Parse args
def get_args(prog=None, argv=None):
    parser = DFNClient.get_std_argparser(prog=prog, quiet=True, step=True, SQL=True, timestamp=True, verbose=True, extended=True)
    parser.add_argument('-S', '--status',          dest='status',          default='ACTIVE',                                                                     help='specify certificate status to be listed (default: ACTIVE; possible values: ACTIVE, APPROVED, ARCHIVED, DELETED, NEW, PENDING)')
    parser.add_argument('-a', '--approved',        dest='status',                                                    const='ARCHIVED',     action='store_const', help='list approved domains only')
    parser.add_argument('-w', '--waiting',         dest='status',                                                    const='ACTIVE',       action='store_const', help='list non-approved (waiting) requests only')
    parser.add_argument('-p', '--progressive',     dest='progressive',     default=False,                                                  action='store_true',  help='print output progressively (default: no)')
    parser.add_argument('requests', metavar='request', nargs='*', type=request,                                                                                  help='request to be listed; if empty, display all requests matching the criteria (default: empty)') 
    return(parser.parse_args(argv))


# Get and print the requests
def get_requests(endpoint_connection, args):
    offset = 0
    object_list = []

    # load the requests
    while True:
        result_list = endpoint_connection.service.searchExtendedItems(Type='request', Status=args.status, Offset=offset, Limit=args.step)
        if not result_list:
            break;
        if not args.quiet and not args.progressive:
            print('.', file=stderr, end="", flush=True)
        if args.extended or args.progressive:
            for object in result_list:
                object['info'] = Toolbox.normalize_request_info(endpoint_connection, args, endpoint_connection.service.getRequestInfo(object['Serial'][0]))
                if not args.quiet and not args.progressive:
                    print('o', file=stderr, end="", flush=True)
                if args.progressive:
                    Toolbox.print_request(args, object)
        if not args.progressive:
            object_list.extend(result_list)
        offset += args.step

    if not args.quiet:
        print(file=stderr, flush=True)
        return(object_list)

    return(object_list)


# Get and print a single request
def get_request(endpoint_connection, args, request):
    try:
        info = endpoint_connection.service.getRequestInfo(request)
        # Because of the interesting SOAP interface model,
        # we have to hack together our own object and fill the
        # relevant fields
        object = Toolbox.create_object_from_requestInfo(endpoint_connection, args, info)
        if args.progressive:
            Toolbox.print_request(args, object)
        else:
            return(object)
    except WebFault as fault:
        print(fault)


# Print a list of requests
def print_requests(args, requests):
    for object in sorted(requests, key=lambda object: object['Serial'][0]):
        Toolbox.print_request(args, object)


# Do the job
def do_job(prog=None, argv=None):
    args = get_args(prog, argv)
    endpoint_connection = DFNClient.connect_endpoint(args)
    object_list = []
    if args.requests:
        for request in args.requests:
            if request == '-':
                for inputline in stdin:
                    request = inputline.rstrip()
                    if request:
                        object = get_request(endpoint_connection, args, request)
                        if object:
                            object_list.extend([object])
            else:
                object = get_request(endpoint_connection, args, request)
                if object:
                    object_list.extend([object])
    else:
        object_list = get_requests(endpoint_connection, args)

    if not args.progressive:
        print_requests(args, object_list)


# Main loop if called directly
if __name__ == "__main__":
    do_job()

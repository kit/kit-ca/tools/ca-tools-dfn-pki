#!/usr/bin/env python

from argparse import ArgumentParser
from ConfigParser import SafeConfigParser
from datetime import datetime
from shutil import move
from sys import argv, exit, stdin
import email
import glob
import json
import ldap
import os
import pipes
import re
import string
import time


# Defaults
rabbitmq_push.DEFAULT_CONFIGFILE = "~/.rabbitmq.cfg"
rabbitmq_push.DEFAULT_EXCHANGE = "processCerts"


def push_new_certificate(pem_string, config):
    """Push a new certificate to the queue.
    Payload: JSON
     - Routing key: "NewCertificate"
     - Type       : String ("Gruppe", "Nutzer", "Pseudonym", "Server")
     - Serial     : Integer
     - Subject    : String
     - CN         : List of string (CN for personal certs, FQHN(s) for server certs)
     - Email      : List of strings
     - NotBefore  : String (ASN1 timestamp)
     - NotAfter   : String (ASN1 timestamp)
     - Hash       : Integer
     - PEM        : String
    """
    
    # Parse the certificate
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, cert_string)

    # Get the obvious information
    cert['Serial'] = certificate.get_serial_number()
    cert['Subject'] = "/" + "/".join(map(lambda T : "=".join(T), certificate.get_subject().get_components()))
    cert['CN'] = [ certificate.get_subject().commonName ]
    if (certificate.get_subject().emailAddress):
        cert['Email'] = [ certificate.get_subject().emailAddress ]
    else:
        cert['Email'] = [ ]
    cert['NotBefore'] = certificate.get_notBefore()
    cert['NotAfter'] = certificate.get_notAfter()
    cert['Hash'] = certificate.subject_name_hash()
    cert['PEM'] = pem_string
    # TODO(tdussa): Parse the SaN Email names as well

    # Determine the certificate type
    if (cert_cn.startsWith("PN:") or
        cert_cn.startsWith("PN - ")):
        cert['Type'] = "Pseudonym"
    elif (cert_cn.startsWith("GRP:") or
          cert_cn.startsWith("GRP - ")):
          cert['Type'] = "Gruppe"
    elif (string.lower(cert_cn).endsWith(".com") or
          string.lower(cert_cn).endsWith(".de") or
          string.lower(cert_cn).endsWith(".edu") or
          string.lower(cert_cn).endsWith(".eu") or
          string.lower(cert_cn).endsWith(".net") or
          string.lower(cert_cn).endsWith(".org")):
        cert['Type'] = "Server"
    else:
        cert['Type'] = "Nutzer"

    # Get a new channel if none has been specified
    if (channel == None):
        credentials = pika.PlainCredentials(
            config.get('RabbitMQ Push', 'login'),
            config.get('RabbitMQ Push', 'password')
        )
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                config.get('RabbitMQ Push', 'hostname'),
                config.get('RabbitMQ Push', 'port'),
                config.get('RabbitMQ Push', 'vhost'),
                credentials, ssl=True
            )
        )
        channel = config.connection.channel()

    # Push the actual message to the exchange
    props = pika.BasicProperties(
        delivery_mode = 2, # make message persistent
        content_type = 'application/json'
    )
    channel.basic_publish(
        exchange = config.get('RabbitMQ Push', 'exchange'),
        routing_key = 'NewCertificate',
        body = json.dumps(cert),
        properties = props
    )


# Parse the arguments
def parse_arguments():
    """Defines and parses the arguments.
    Returns the argument namespace.
    """

    # Parse args
    parser = ArgumentParser()
    parser.add_argument(
        "--config-file",
        dest="config_file",
        default=rabbitmq_push.DEFAULT_CONFIGFILE,
        help="Alternative configuration file (default: " + 
             rabbitmq_push.DEFAULT_CONFIGFILE + ")"
    )
    parser.add_argument(
        "--exchange",
        dest="exchange",
        default=None,
        help="Exchange to use (default: " +
             rabbitmq_push.DEFAULT_EXCHANGE +
             " if nothing set in config file)")
    return(parser.parse_args())
    

# Read config file
def read_config_file(args):
    """Reads the config file.
    Takes an argument namespace as input.
    Returns a SafeConfigParser instance with the config variables set.
    """

    # Load the ConfigParser and set default values
    config = SafeConfigParser()
    config.add_section('RabbitMQ Push')
    config.set('RabbitMQ Push', 'exchange', rabbitmq_push.DEFAULT.EXCHANGE)
    config.read(os.path.expanduser(args.configFile))

    # Read default exchange and figure out which exchange to use
    if args.exchange:
        config.set('RabbitMQ Push', 'exchange', args.exchange)

    # Sanity checks
    if not config.has_option('RabbitMQ Push', 'host'):
        print("Missing config value: [RabbitMQ Push]/host!")
        exit(1)
    if not config.has_option('RabbitMQ Push', 'port'):
        print("Missing config value: [RabbitMQ Push]/port")
        exit(2)
    if not config.has_option('RabbitMQ Push', 'vhost'):
        print("Missing config value: [RabbitMQ Push]/vhost!")
        exit(3)
    if not config.has_option('RabbitMQ Push', 'login'):
        print("Missing config value: [RabbitMQ Push]/login!")
        exit(4)
    if not config.has_option('RabbitMQ Push', 'password'):
        print("Missing config value: [RabbitMQ Push]/password!")
        exit(5)
    if not config.has_option('RabbitMQ Push', 'exchange'):
        print("Missing config value: [RabbitMQ Push]/exchange!\n" +
              "(Can also be specified as a command line option.)")
        exit(6)

    return(config)


# Main code if called directly.
def main():
    """Main program code if not sourced as a library.
    Pulls in argument parser and expects a PEM file from stdin.
    """
 

# If called directly, run the main code
if __name__ == '__main__':
    main()

package edu.kit.cert;

import java.io.DataInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CRLException;
import java.security.cert.X509CRLEntry;

import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.io.pem.PemObject;

import sun.security.util.ObjectIdentifier;
import sun.security.x509.Extension;
import sun.security.x509.X509CRLImpl;
import sun.security.x509.X509CertImpl;

import de.dfncert.soap.DFNCERTRegistration;
import de.dfncert.tools.DFNPKIClient;

public class CreateList {

    /**
     * @param args
     */
    public static void main(String[] args) {

        // Your private key for the authentication.
        String certFile = "";

        // Your password to unlock the private key.
        String password = "";

        // The name of the CA.
        String ca = "kit-ca";

        // The SOAP client.
        DFNPKIClient client;

        // The KIT-RA.
        DFNCERTRegistration registration;

        // The file writer.
        PrintWriter out;

        if(args.length <= 1)
        {
            printUsage();
            return;
        }

        // Parse the command line arguments.
        for (int i = 0; i < args.length - 1; i++) {
            switch(args[i]) {
            /*case "-c":
                i++;
                ca = args[i];
                break;*/
            case "-C":
                i++;
                certFile = args[i];
                break;
            case "-p":
                i++;
                password = args[i];
                break;
            default: System.out.println("Unknown option: " + args[i]);
            }
        }

        if(ca.isEmpty() || certFile.isEmpty())
        {
            printUsage();
            return;
        }

        // Connect to the DFN-PKI
        try {
            client = new DFNPKIClient(ca);
        } catch (Exception e) {
            System.out.println("Error connecting to ca: " + e);
            return;
        }

        try {
            client.loadRAFromPKCS12(certFile, password.toCharArray());
        } catch (Exception e) {
            System.out.println("Error loading certificate: " + e);
            return;
        }

        registration = client.getRegistration();

        try {
            out = new PrintWriter(new FileWriter("crlList.txt"));
        } catch (IOException e2) {
            System.out.println("Error creating crlList.txt : " + e2);
            return;
        }

        // Write the CRL
        try {
            byte[] crl = downloadCRL(new URL("http://cdp1.pca.dfn.de/kit-ca/pub/crl/cacrl.crl"));

            if(crl.length == 0)
            {
                System.out.println("Error: CRL is empty.");
            }

            PEMReader reader;
            PemObject pemObj;
            X509CertImpl cert;
            Extension ext;

            for(BigInteger serial : getSerialsFromCRL(crl))
            {
                System.out.print(serial + " => ");
                reader = new PEMReader(new StringReader(registration.getCertificate(serial)));
                pemObj = reader.readPemObject();
                reader.close();

                cert = new X509CertImpl(pemObj.getContent());
                ext = cert.getExtension(new ObjectIdentifier("2.5.29.17"));
                if(ext != null) {
                    String[] splitExt = ext.toString().split("\n");
                    String mail = "";
                    for(String extLine : splitExt) {
                        if(extLine.contains("RFC822Name:")) {
                            mail += extLine.substring(14, extLine.length()) + ";";
                        }
                    }

                    if(!mail.isEmpty()) {
                        out.println(serial + "|" + mail);
                        System.out.println(mail);
                    } else {
                        System.out.println("No RFC822Name.");
                    }
                } else {
                    System.out.println("No mail extention.");
                }
            }
        } catch (IOException e1) {
            System.out.println("Error downloading the crl: " + e1);
            return;
        } catch (CRLException e) {
            System.out.println("Error parsing the crl: " + e);
            return;
        }catch (Exception e) {
            System.out.println("Error requesting certificate info: " + e);
            return;
        }
        finally {
            out.close();
        }

        System.out.println("Done.");
    }

    /*
     * Download the Certificate Revocation List to memory.
     */
    public static byte[] downloadCRL(URL location) throws IOException {

        if(location == null)
        {
            return new byte[0];
        }

        URLConnection connection = location.openConnection();
        DataInputStream input =
                new DataInputStream(connection.getInputStream());
        byte[] file = new byte[connection.getContentLength()];

        System.out.println("Download " + file.length / 1024 + "kB CRL from " + location.toString());

        for (int i = 0; i < file.length; i++) {
            file[i] = input.readByte();
        }

        input.close();

        System.out.println("Done!");
        return file;
    }

    /*
     * Get a list of serials from the crl.
     */
    public static BigInteger[] getSerialsFromCRL(byte[] crlData) throws CRLException
    {
        System.out.println("Get Serials from CRL.");
        X509CRLImpl crl = new X509CRLImpl(crlData);
        Object[] revokedCerts = crl.getRevokedCertificates().toArray();
        BigInteger[] serialList = new BigInteger[revokedCerts.length];
        for (int i = 0; i < serialList.length; i++) {
            serialList[i] = ((X509CRLEntry)revokedCerts[i]).getSerialNumber();
        }
        System.out.println("Found " + serialList.length + " serials." );
        return serialList;
    }

    /**
     * Print the usage.
     */
    public static void printUsage() {
        System.out.println("Usage:\n\t<-c> <CA name>\n\t<-C> <certificate file>\n\t<-p> <certificate password>");
    }

}

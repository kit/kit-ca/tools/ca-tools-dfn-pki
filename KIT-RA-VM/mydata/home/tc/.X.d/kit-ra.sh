#!/bin/sh

# Give the network time to settle
sleep 5

# Load the JavaWS interface
javaws https://pki.pca.dfn.de/kit-ca/guira/guira.jnlp

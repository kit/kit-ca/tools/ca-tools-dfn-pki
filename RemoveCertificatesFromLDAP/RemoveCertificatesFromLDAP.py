#!/usr/bin/python

from sys import argv, stdin
import os
import ldap
import base64
from OpenSSL import crypto
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser

# Parse args
parser = ArgumentParser()
parser.add_argument("arguments", nargs="*", help="arguments to the operation")
args = parser.parse_args()


# Read config file
config = SafeConfigParser({'config': None, 'datafile': None, 'version': '3', 'timeout': '5'})
config.read(os.path.expanduser('~/.RemoveCertificatesFromLDAP.cfg'))


# Set up LDAP parameters
ldaphandle = ldap.initialize(config.get('LDAP', 'server'))
ldaphandle.set_option(ldap.OPT_REFERRALS, 0)
ldaphandle.set_option(ldap.OPT_NETWORK_TIMEOUT, 5)
ldaphandle.set_option(ldap.OPT_TIMEOUT, 5)



# Read certificate string
def readCertificate(input):
    # Open the proper input stream
    if input == "-":
        inputStream = stdin
    else:
        inputStream = open(input)

    # Read the certificate string
    certificateString = ""
    for inputLine in inputStream:
        certificateString += inputLine
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, certificateString)

    # Close the input stream if necessary
    if inputStream is not stdin:
        inputStream.close()

    # Return the result
    return(certificateString, certificate)


# Remove certificate from LDAP
def removeCertificate(certificateString, certificate):
    try:
        ldaphandle.protocol_version = config.getint('LDAP', 'version')
        ldaphandle.simple_bind_s(config.get('LDAP', 'username'), config.get('LDAP', 'password'))
        search_filter = "(proxyAddresses=smtp:%s)" % (certificate.get_subject().emailAddress)
        # Parse the email address
        for index in range(certificate.get_extension_count()):
            extension = certificate.get_extension(index)
            if (extension.get_short_name() == "subjectAltName"):
                print extension.get_data()
                break

        print search_filter
        results = ldaphandle.search_s(config.get('LDAP', 'base'), ldap.SCOPE_SUBTREE, search_filter)
        for entry in filter(lambda x: ((x[0] != None)), results):
            print "DN: "
            print "DN: " + entry[1].dn 
        #for entry in filter(lambda x: ((x[0] != None) and (x[1].has_key('dn'))), results):
        print "Number of results: " + str(len(results))

    
    except Exception, error:
        print error


for input in args.arguments:
    # Read the certificate
    (certificateString, certificate) = readCertificate(input)

    # Process the certificate
    removeCertificate(certificateString, certificate)




def testrun():
    try:
        ldaphandle.protocol_version = config.getint('LDAP', 'version')
        ldaphandle.simple_bind_s(config.get('LDAP', 'username'), config.get('LDAP', 'password'))
        results = ldaphandle.search_s(config.get('LDAP', 'base'), ldap.SCOPE_SUBTREE, search_filter, ['userCertificate', 'userSMIMECertificate'])
        for entry in filter(lambda x: ((x[0] != None) and ((x[1].has_key('userCertificate') or (x[1].has_key('userSMIMECertificate'))))), results):
            safeEntry = dict({'userCertificate': [''], 'userSMIMECertificate': ['']}.items() + entry[1].items())
            for certificate in safeEntry['userCertificate']:
                if (len(certificate) > 0):
                    print("-----BEGIN CERTIFICATE-----")
                    base64cert = base64.b64encode(certificate)
                    for i in range(0, len(base64cert), 72):
                        print(base64cert[i:i+72])
                    print("-----END CERTIFICATE-----")
            for certificate in safeEntry['userSMIMECertificate']:
                if (len(certificate) > 0):
                    print("-----BEGIN PKCS7-----")
                    base64cert = base64.b64encode(certificate)
                    for i in range(0, len(base64cert), 72):
                        print(base64cert[i:i+72])
                    print("-----END PKCS7-----")
    
    except Exception, error:
        print error

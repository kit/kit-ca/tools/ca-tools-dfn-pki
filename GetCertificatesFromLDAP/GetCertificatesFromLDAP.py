#!/usr/bin/python
# $Id: mutt-query.py 567 2013-03-28 12:04:06Z tdussa $
# $Author: tdussa $
# $Revision: 567 $
# $Date: 2013-03-28 13:04:06 +0100 (Thu, 28 Mar 2013) $
# $URL: svn+ssvn://tdussa@svn.dussa.de/svn/jeep/misc/bin/mutt-query.py $

from sys import argv
import os
import ldap
import base64
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser

# Parse args
parser = ArgumentParser()
parser.add_argument("filter",                    type=str,                          help="search filter")
args = parser.parse_args()


# Read config file
config = SafeConfigParser({'config': None, 'datafile': None, 'version': '3', 'timeout': '5'})
config.read(os.path.expanduser('~/.GetCertificatesFromLDAP.cfg'))

search_filter = "(proxyAddresses=smtp:%s)" % (args.filter)

ldaphandle = ldap.initialize(config.get('LDAP', 'server'))
ldaphandle.set_option(ldap.OPT_REFERRALS, 0)
ldaphandle.set_option(ldap.OPT_NETWORK_TIMEOUT, 5)
ldaphandle.set_option(ldap.OPT_TIMEOUT, 5)

try:
    ldaphandle.protocol_version = config.getint('LDAP', 'version')
    ldaphandle.simple_bind_s(config.get('LDAP', 'username'), config.get('LDAP', 'password'))
    results = ldaphandle.search_s(config.get('LDAP', 'base'), ldap.SCOPE_SUBTREE, search_filter, ['userCertificate', 'userSMIMECertificate'])
    for entry in filter(lambda x: ((x[0] != None) and ((x[1].has_key('userCertificate') or (x[1].has_key('userSMIMECertificate'))))), results):
        safeEntry = dict({'userCertificate': [''], 'userSMIMECertificate': ['']}.items() + entry[1].items())
        for certificate in safeEntry['userCertificate']:
            if (len(certificate) > 0):
                print("-----BEGIN CERTIFICATE-----")
                base64cert = base64.b64encode(certificate)
                for i in range(0, len(base64cert), 72):
                    print(base64cert[i:i+72])
                print("-----END CERTIFICATE-----")
        for certificate in safeEntry['userSMIMECertificate']:
            if (len(certificate) > 0):
                print("-----BEGIN PKCS7-----")
                base64cert = base64.b64encode(certificate)
                for i in range(0, len(base64cert), 72):
                    print(base64cert[i:i+72])
                print("-----END PKCS7-----")

except Exception, error:
    print error

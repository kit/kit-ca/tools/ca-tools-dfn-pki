# CA Toolbox module file


# Import libs
from argparse import ArgumentTypeError
from suds import WebFault
from time import strftime, strptime
import locale


# Define output format(s)
request_output = {'list': ' + {0}\t{1}\t{2}\t{3}\t{4}',
                  'sql': 'INSERT INTO requests VALUES ({0}, \'{1}\', \'{2}\', \'{3}\', datetime(\'{4}\'));',
                  'verbose': '\
 + Request #{0}:\n\
     Distinguished name: {1}\n\
     E-mail address:     {2}\n\
     Profile:            {3}\n\
     Submission date:    {4}\n'}
request_output_extended = {'list': ' + {0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}',
                           'sql': 'INSERT INTO requests VALUES ({0}, \'{1}\', \'{2}\', \'{3}\', \'{4}\', datetime(\'{5}\'), \'{6}\', \'{7}\', \'{8}\', \'{9}\', \'{10}\', \'{11}\', \'{12}\', \'{13}\', \'{14}\', \'{15}\', \'{16}\', \'{17}\');',
                           'verbose': '\
 + Request #{0} (RA ID {1}):\n\
     Distinguished name:    {2}\n\
     E-mail address:        {3}\n\
     SaN(s):                {4}\n\
     Profile:               {5}\n\
     Submission date:       {6}\n\
     Requested by:          {7} {8}<{9}>\n\
     RSA Key Size:          {10}\n\
     Fingerprint:           {11}\n\
     To be published:       {12}\n\
     Approval date:         {13}\n\
     Deletion date:         {14}\n\
     Issued certificate:    {15}\n\
     Matching certificates: {16}\n\
     Status:                {17}\n'}


# Define input validation routines
# Accept ints and dashes
def is_int_or_dash(input):
    if input != '-':
        try:
            if int(input) <= 0:
                raise(ArgumentTypeError('"{0}" is not a valid input (int or dash)'.format(input)))

        except ValueError:
            raise(ArgumentTypeError('"{0}" is not a valid input (int or dash)'.format(input)))

    return input


# Accept ints, comma-separated int-and-hex tuples, and dashes
def is_int_int_hex_or_dash(input):
    if input != '-':
        try:
            output = (input+',').split(',')

            # Make sure the first part is a positive int
            if int(output[0]) <= 0:
                raise(ArgumentTypeError('"{0}" is not a valid input (int, int-hex-tuple, or dash)'.format(input)))

            # Make sure the second part is empty or a hex string
            if len(output[1]) > 0:
                output[1] = output[1].replace(':', '').upper()
                int(output[1], 16)
        except ValueError:
            raise(ArgumentTypeError('"{0}" is not a valid input (int, int-hex-tuple, or dash)'.format(input)))
    else:
        output = input

    return output


# Normalize certificate request information
#  + Convert DateSubmitted, DateApproved, DateDeleted into the string format
#    as specified on the command line
#  + Move the current request's certificate (if any) from the SameDNSerial list
#    into its own field iff extended information is requested
def normalize_request_info(endpoint_connection, args, info):
    # Date field normalization
    old_locale = locale.setlocale(locale.LC_TIME)
    for dateField in ['DateSubmitted', 'DateApproved', 'DateDeleted']:
        if info[dateField]:
            locale.setlocale(locale.LC_TIME, 'C')
            tmpTime = strptime(info[dateField], '%a %b %d %H:%M:%S %Y %Z')
            locale.setlocale(locale.LC_TIME, old_locale)
            info[dateField] = strftime(args.timestamp, tmpTime)

    # Certificate serial normalization iff extended information is requested
    info['CertificateSerial'] = None
    if args.extended:
        for serial in info['SameDNSerials']:
            try:
                certificateInfo = endpoint_connection.service.getCertificateInfo(serial)
                if (certificateInfo['RequestSerial'] == info['Serial']):
                    info['CertificateSerial'] = serial
                    newList = info['SameDNSerials']
                    newList.remove(serial)
                    info['SameDNSerials'] = newList
                    break
            except WebFault as fault:
                print(fault)

    return(info)


# Create a DFNCERTTypesObject out of a DFNCERTTypesRequestInfo
def create_object_from_requestInfo(endpoint_connection, args, info):
    object = {
              'Serial': [info['Serial']],
              'RaID': [info['Parameters']['RaID']],
              'Subject': [info['Parameters']['Subject']],
              'Role': [info['Parameters']['Role']],
              'Date': [info['Parameters']['NotBefore']],
              'info': normalize_request_info(endpoint_connection, args, info)
             }
    for SaN in info['Parameters']['SubjectAltNames']:
        if SaN.startswith('email:'):
            object['EMail'] = [SaN[6:]]
    if not 'EMail' in object:
        object['EMail'] = ['']

    return(object)


# Prettyprint certificate request
def print_request(args, object):
    if (args.extended):
        # Normalize additionalName, additionalUnit and additionalEMail
        additionalUnit = ''
        if object['info']['Parameters']['AdditionalName']:
            additionalName = object['info']['Parameters']['AdditionalName']
        else:
            additionalName = object['Subject'][0]
            if not additionalName.startswith('CN='):
                additionalName = ''
            else:
                additionalName = additionalName[3:]
                index = additionalName.find(',O=')
                if index > 0:
                    additionalName = additionalName[:index]
                index = additionalName.find(',OU=')
                if index > 0:
                    additionalUnit = additionalName[index+4:]
                    additionalName = additionalName[:index]

        if object['info']['Parameters']['AdditionalUnit']:
            additionalUnit = object['info']['Parameters']['AdditionalUnit']

        if object['info']['Parameters']['AdditionalEMail']:
            additionalEMail = object['info']['Parameters']['AdditionalEMail']
        else:
            additionalEMail = object['EMail'][0]

        if additionalUnit:
            additionalUnit = '(' + additionalUnit + ') '

        print(request_output_extended[args.output_format].format(
            object['Serial'][0],
            str(object['RaID'][0]),
            object['Subject'][0],
            object['EMail'][0],
            ', '.join(object['info']['Parameters']['SubjectAltNames']),
            object['Role'][0],
            object['Date'][0].strftime(args.timestamp),
            additionalName,
            additionalUnit,
            additionalEMail,
            str(object['info']['PublicKeyLength']),
            object['info']['PublicKeyDigest'],
            object['info']['Publish'],
            object['info']['DateApproved'],
            object['info']['DateDeleted'],
            object['info']['CertificateSerial'],
            ', '.join(map(str, object['info']['SameDNSerials'])),
            object['info']['Status']
        ))
    else:
        print(request_output[args.output_format].format(
            object['Serial'][0],
            object['Subject'][0],
            object['EMail'][0],
            object['Role'][0],
            object['Date'][0].strftime(args.timestamp)
        ))

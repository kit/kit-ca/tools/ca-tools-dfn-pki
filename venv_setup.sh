#!/bin/sh

VENVDIR=.env

python3 -m venv ${VENVDIR}

${VENVDIR}/bin/pip3 install -r requirements.txt

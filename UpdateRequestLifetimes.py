#!.env/bin/python3

# Import libs
from argparse import ArgumentParser
from datetime import datetime, timedelta
from dateutil import parser
from suds import WebFault
from sys import stdin
import DFNClient
import Toolbox
import re


# Set some constants
description = 'update lifetimes for certificate requests'

evil_dates = [
              '-01-01', '-01-02', '-01-03', '-01-04', '-01-05', '-01-06',
              '-05-01',
              '-10-03',
              '-11-01',
              '-12-23', '-12-24', '-12-25', '-12-26', '-12-27', '-12-28', '-12-29', '-12-30', '-12-31',
             ] 

lifetime_limits = {
                   '802.1X Client':         397,
                   '802.1X User':          1095,
                   'Code Signing':         1095,
                   'Domain Controller':     397,
                   'Exchange Server':       397,
                   'LDAP Server':           397,
                   'Mail Server':           397,
                   'OCSP Responder':        300,
                   'Radius Server':         397,
                   'RA Operator':          1095,
                   'Shibboleth IdP SP':     397,
                   'User':                 1095,
                   'UserEncrypt':          1095,
                   'UserSignAndLogon':     1095,
                   'UserSign':             1095,
                   'VoIP Server':           397,
                   'VPN Server':            397,
                   'Web Server':            397,
                   'Webserver MustStaple':  397,
                  }   
lifetime_limits_default = 1095


# Accept int or a dash
def request(input):
    return Toolbox.is_int_or_dash(input)


# Parse args
def get_args(prog=None, argv=None):
    parser = DFNClient.get_std_argparser(prog=prog, dryrun=True, quiet=True, step=False, SQL=False, timestamp=True, verbose=True, extended=True)
    parser.add_argument('-l', '--lifetime', dest='lifetime', type=int, help='specify the maximum number of days of desired certificate lifetime (default is 397 or 1095, depending on certificate type)')
    parser.add_argument('-L', '--exact-lifetime', dest='exact_lifetime', type=int, help='specify the exact number of days of desired certificate lifetime without further sanity reductions to last Tuesday or anything (default is 397 or 1095, depending on certificate type)')
    parser.add_argument('-T', '--exact-timestamp', dest='exact_timestamp', type=str, help='specify the exact timestamp of the desired certificate end-of-life time (in ISO8601 format; takes precedence over lifetime options)')
    parser.add_argument('requests', metavar='request', nargs='+', type=request, help='request to be updated') 
    return(parser.parse_args(argv))


# Verify and print a request
def update_request(endpoint_connection, args, request):
    try:
        # Retrieve request info
        request_info = endpoint_connection.service.getRequestInfo(request)

        # If exact-timestamp is set, set NotAfter to that; use lifetime
        # otherwise
        if args.exact_timestamp:
            not_after = parser.parse(args.exact_timestamp)
        else:
            # If exact-lifetime is set, set NotAfter to that timestamp
            # Calculate and set NotAfter timestamp otherwise
            if args.exact_lifetime:
                not_after = datetime.now().replace(hour=10, minute=0, second=0, microsecond=0) \
                            + timedelta(days=args.exact_lifetime)
            else:
                # Starting timestamp is $TODAY plus lifetime limit minus 1
                if args.lifetime:
                    lifetime = args.lifetime
                else:
                    lifetime = lifetime_limits.get(request_info['Parameters']['Role'], lifetime_limits_default)
                not_after = datetime.now().replace(hour=10, minute=0, second=0, microsecond=0) \
                            + timedelta(days=(lifetime-1))
                # Move backwards to the last Tuesday
                not_after = not_after + timedelta(days=-((not_after.weekday()-1)%7))
                # While the resulting date is in the evil date list, move back another week
                while any(evil_date in str(not_after.date()) for evil_date in evil_dates):
                    not_after = not_after + timedelta(days=-7)

        request_info['Parameters']['NotAfter'] = not_after

        print('Request #' + request + ': Setting NotAfter timestamp to ' + not_after.strftime(args.timestamp) + '... ', end='', flush=True)
        if args.dryrun:
            print('DRYRUN SELECTED.  NOT ACTUALLY CHANGING ANYTHING.')
        else:
            if not endpoint_connection.service.setRequestParameters(request_info['Serial'], request_info['Parameters']):
                print('ERROR WHILE UPDATING CERTIFICATE REQUEST!')
            else:
                print('Done.')
    except WebFault as fault:
        print(fault)


# Do the job
def do_job(prog=None, argv=None):
    args = get_args(prog, argv)
    endpoint_connection = DFNClient.connect_endpoint(args)
    object_list = []
    for request in args.requests:
        if request == '-':
            for inputline in stdin:
                request = inputline.rstrip()
                if request:
                    update_request(endpoint_connection, args, request)
        else:
            update_request(endpoint_connection, args, request)


# Main loop if called directly
if __name__ == "__main__":
    do_job()

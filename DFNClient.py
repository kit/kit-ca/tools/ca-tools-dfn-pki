# DFN SOAP API module file


# Import libs
from argparse import ArgumentParser
from io import BytesIO
from suds.client import Client
from suds.transport import Reply, TransportError
from suds.transport.http import HttpAuthenticated
import requests


# Define standard endpoint URL templates
endpoint_template = 'https://ra.pca.dfn.de/{CA}/cgi-bin/ra/soap/DFNCERT/{endpoint}'
unprivileged_endpoint_template = 'https://pki.pca.dfn.de/{CA}/cgi-bin/pub/soap/DFNCERT/{endpoint}'


# Connect to DFN SOAP endpoint
def connect_endpoint(args):
    return(get_client(CA=args.CA, certificate=args.certificate))
    

# Define HTTPS transport class w/client certificate authentication
class RequestsTransport(HttpAuthenticated):
    def __init__(self, **kwargs):
        self.cert = kwargs.pop('cert', None)
        # super won't work because not using new style class
        HttpAuthenticated.__init__(self, **kwargs)

    def open(self, request):
        """
        Fetches the WSDL using cert.
        """
        self.addcredentials(request)
        resp = requests.get(request.url, data=request.message, verify='Root-CA.pem',
                             headers=request.headers, cert=self.cert)
        result = BytesIO(resp.content)
        return result

    def send(self, request):
        """
        Posts to service using cert.
        """
        self.addcredentials(request)
        resp = requests.post(request.url, data=request.message, verify='Root-CA.pem',
                             headers=request.headers, cert=self.cert)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result


# Define function to get a client connection
def get_client(certificate, CA, endpoint='Register?wsdl=1'):
    if (certificate == None):
        print('ERROR: No certificate specified.')
        raise ValueError('No certificate specified.')

    if (CA == None):
        print('ERROR: No CA specified.')
        raise ValueError('No CA specified.')
        #from OpenSSL import crypto
        #from pyasn1.codec.der import decoder as der_decoder
        #from pyasn1_modules import rfc2459
        #cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(certificate, 'rt').read())
        #for ext in range(0, cert.get_extension_count()):
        #    Ext = cert.get_extension(ext)
        #    if (Ext.get_short_name().decode('utf8') == 'authorityInfoAccess'):
        #        print('Yes')
        #        decoded_dat, a = der_decoder.decode(Ext.get_data(), asn1Spec=rfc2459.Extensions())
        #        print(decoded_dat.
        #        print('yesend')

    headers = {"Content-Type" : "text/xml;charset=UTF-8"}
    t = RequestsTransport(cert=certificate)
    return(Client(endpoint_template.format(CA=CA, endpoint=endpoint), headers=headers, transport=t))


# Define function to get an unprivileged client connection
def get_unprivileged_client(CA, endpoint='Public?wsdl=1'):
    if (CA == None):
        print('ERROR: No CA specified.')
        raise ValueError('No CA specified.')

    headers = {"Content-Type" : "text/xml;charset=UTF-8"}
    return(Client(unprivileged_endpoint_template.format(CA=CA, endpoint=endpoint), headers=headers))


# Provide standard argument parser with some sensible defaults
def get_std_argparser(prog=None,
                      certificate=True,
                      CA=True,
                      dryrun=False,
                      passphrase=False, # functionality not yet enabled
                      passphrase_file=False, # functionality not yet enabled
                      RA=False,
                      quiet=False,
                      step=False,
                      SQL=False,
                      timestamp=False,
                      verbose=False,
                      extended=False):
    parser = ArgumentParser()

    output_list = ['list']
    if SQL:
        output_list.append('sql')
    if verbose:
        output_list.append('verbose')
    output_list = sorted(output_list)

    if certificate:
        parser.add_argument('-c', '--certificate',     dest='certificate',     default='RA-Operator-Cert.pem',                                                  help='specify RA operater certificate file in PEM format (default: RA-Operator-Cert.pem)')
    if CA:
        parser.add_argument('-C', '--ca',              dest='CA',                                                                                               help='specify CA name (default: none)')
    if dryrun:
        parser.add_argument('-d', '--dryrun',          dest='dryrun',          default=False,                                             action='store_true',  help='dry run (do not actually change anything; default: no)')
    if passphrase:
        parser.add_argument('-p', '--passphrase',      dest='passphrase',                                                                                       help='specify the certificate passphrase (takes precedence over --passphrase-file)')
    if passphrase_file:
        parser.add_argument('-P', '--passphrase-file', dest='passphrase_file',                                                                                  help='specify a file containing the certificate passphrase')
    if RA:
        parser.add_argument('-R', '--ra',              dest='RA',              default=0,                      type=int,                                        help='specify the RA ID (default: 0)')
    if step:
        parser.add_argument('-T', '--step',            dest='step',            default=50,                     type=int,                                        help='specify search stepping (default: 50)')
    if extended:
        parser.add_argument('-x', '--extended',        dest='extended',        default=False,                                             action='store_true',  help='list extended information (default: no)')
    if timestamp:
        parser.add_argument('-t', '--timestamp',       dest='timestamp',       default='%Y-%m-%dT%H:%M:%SZ',                                                    help='specify timestamp format (default: %%Y-%%m-%%dT%%H:%%M:%%SZ)')
    if quiet:
        parser.add_argument('-q', '--quiet',           dest='quiet',           default=False,                                             action='store_true',  help='make output quiet (default: no)')
    if (len(output_list) > 1):
        parser.add_argument('-O', '--output-format',   dest='output_format',   default='list',                 choices=output_list,                             help='specify output format (default: list)')
        if SQL:
            parser.add_argument('-Q', '--sql',         dest='output_format',                                   const='sql',               action='store_const', help='generate SQL output (same as "--output-format sql")')
        if verbose:
            parser.add_argument('-V', '--verbose',     dest='output_format',                                   const='verbose',           action='store_const', help='generate verbose (human-readable) output (same as "--output-format verbose")')
    return(parser)

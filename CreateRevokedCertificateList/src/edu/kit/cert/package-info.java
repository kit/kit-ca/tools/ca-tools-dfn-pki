/**
 * The Software is used to build a list of revoked certificate serials with a matching to their RFC822Name E-Mail addresses.
 */
/**
 * @author weis
 *
 */
package edu.kit.cert;